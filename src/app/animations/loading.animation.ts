import {trigger, style, animate, query, stagger, transition} from '@angular/animations';

export const loadingAnimation = function(){
    return trigger('loading', [ //identificador
        transition('* => *', [
            query(':leave',[
                stagger(100, [ //tiempo de retraso
                    animate('350ms', style({opacity: 0})) // total de la duracion
                ]) //retraso
            ], {optional:true}),
            query(':enter',[
                style({opacity: 0}),
                stagger(100, [ //tiempo de retraso
                    animate('350ms', style({opacity: 1})) // total de la duracion
                ]) //retraso
            ], {optional:true})
        ])
    ])
}