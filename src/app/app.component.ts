import { Component } from '@angular/core';
import { ForecastService } from './services/forecast.service';
import { GeolocationService } from './services/geolocation.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'weatherapi';

  constructor(private geolocationService: GeolocationService){}

  ngOnInit(): void {
    // this.forecasteService.weather$.subscribe(console.log)
    this.geolocationService.requestGeolocation();
  }
}
